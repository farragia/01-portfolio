<?php
	session_start();
	if(!$_SESSION['flag']){ //if login in session is not set
    		header("location:login.php");
    		die;
		}
	function connectDB() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "tugasakhir";
		
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " + mysqli_connect_error());
		}
		return $conn;
	}
	
	function selectAllFromTable($table) {
		$conn = connectDB();
		
		$sql = "SELECT * FROM $table";
		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}

	function kembaliBuku($id){
		$conn = connectDB();
		$user_id = $_SESSION['id_user'];
		$sql = "SELECT * FROM loan WHERE book_id = '$id' and user_id = '$user_id'";
		$result = mysqli_query($conn, $sql);	

		while ($row = mysqli_fetch_row($result)) {
			if($row[1] == $id && $row[2] == $user_id) {
				$sql2 = "SELECT book_id, quantity FROM book WHERE book_id = $id";
				$result2 = mysqli_query($conn, $sql2);
				$row2 = mysqli_fetch_array($result2);
				$book_id = $row2[0];
				$quantity = $row2[1] + 1;	
				$sql3 = "UPDATE book SET quantity = '$quantity' WHERE book_id = $book_id";
				$result3 = mysqli_query($conn, $sql3);
				

				$sql4 = "DELETE FROM loan WHERE book_id = $book_id";
				$result4 = mysqli_query($conn, $sql4);
			}
		}
		mysqli_close($conn);
	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if($_POST['command'] === 'insert') {
			insertBook();
		} else if($_POST['command'] === 'kembalikan') {
			kembaliBuku($_POST['book_id']);
		} else if($_POST['command'] === 'pinjam') {
			pinjamBuku($_POST['book_id']);
		}
	}
	
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>BookaBookoo</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">

		<style type="text/css">
			#table2{
				background-color: white;
			}
		</style>
	</head>
	<body>
		<nav class="cyan darken-2">
			<div class="nav-wrapper">
				<img src="src/Bookabookoo.png" class="brand-logo" alt="buku" width="300">
				<ul class="right">
					<li><a href="bukuUser.php">Home</a></li>
					<li><a href="logout.php">Sign Out</a></li>
				</ul>
			</div>
		</nav>
		<br>
		<style type="text/css">
			.daftar{
				background-color: black;
				color: white;
				padding : 5px;
			}
		</style>
			<div class="table-responsive">
				<table class='table' id="table2">
					<thead> <tr><th>Image</th> <th>Tittle</th> <th>Author</th> <th>Publisher</th><th>Quantity</th></tr> </thead>
					<tbody>
						<?php
							$conn = connectDB();
							$id = $_SESSION['id_user'];
							$sql = "SELECT * FROM loan WHERE user_id='$id'";
							$result = mysqli_query($conn, $sql);

							while ($row = mysqli_fetch_row($result)) {
								
								$book = $row[1];
								$sql2 = "SELECT * FROM book WHERE book_id = '$book'";
								$result2 = mysqli_query($conn, $sql2);
								$row2 = mysqli_fetch_row($result2);

								echo "<tr>";
									echo "<td><img src=\"$row2[1]\" width=\"70%\" /></td>";
									echo "<td><a href=deskripsi.php?id=".$row[0].".php>".$row2[2]."</a></td>";
									echo "<td>".$row2[3]."</td>";
									echo "<td>".$row2[4]."</td>";
									echo "<td>".$row2[6]."</td>";
								echo '<td>
								<form action = "peminjaman.php" method="post">
								<input type="hidden" id="kembali" name="book_id" value="'.$row2[0].'">
								<input type="hidden" id="kembali-command" name="command" value="kembalikan">
								<button type="submit" class="btn-danger">Kembalikan</button></form>
								</td>';
								}
								echo "</tr>";
						?>
					</tbody>
				</table>
			</div>
			<br />
			<br />
			<br />
			
			</div>
		</div>
		
		<style type="text/css">
			 .btn btn-primary {
			 	text-align: center;
			 }
			 
		</style>
		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<div class="footer-bottom">
		<style type="text/css">
			.btn {
				background-color: orange;
				border: none;
			}
			.btn a:hover {
				background-color: black

			}
		</style>

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="copyright">
						© 2016, Bookabookoo, All rights reserved
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="design">
						 <a href="#">Bookabookoo </a> |  <a target="_blank" href="http://www.scele.cs.ui.ac.id">Web Design & Development by Bookabookoo</a>
					</div>
				</div>
			</div>
		</div>

	</div>
	</body>
</html>