<?php
	session_start();
	if(!$_SESSION['flag']){ //if login in session is not set
    		header("location:login.php");
    		die;
		}
	function connectDB() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "tugasakhir";
		
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " + mysqli_connect_error());
		}
		return $conn;
	}
	
	function selectAllFromTable($id) {
		 $conn = connectDB();
		 $sql = "SELECT * FROM book WHERE book_id =".$_GET['id'];

		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}

	function selectAllFromUser($id) {
		 $conn = connectDB();
		 $sql = "SELECT * FROM user WHERE user_id =".$_GET['id'];

		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}



	function selectAllRev($id) {
		$conn = connectDB();
		
		$sql = "SELECT book_id, username, date, content FROM review r, user u WHERE book_id=".$_GET['id']." AND r.user_id=u.user_id";
		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}
	
	function setReview(){
		if (isset($_POST['reviewSubmit'])) {
			# code...
		$conn = connectDB();
		
		
		$book_id = $_POST['book_id'];
		$user_id = $_POST['user_id'];
		$content= $_POST['content'];
		
		$sql = "INSERT into review (book_id, user_id, content, date) values('$book_id','$user_id','$content', curdate())";
		
		if($result = mysqli_query($conn, $sql)) {
			echo "New record created successfully <br/>";
			header("Location: deskripsi.php?id=$book_id");
			} else {
			die("Error: $sql");
		}
		mysqli_close($conn);
		}
	}

	function getReview(){

		$review = selectAllRev($_GET['id']);
		$userid = $_SESSION['login_user'];
		
		while ($row = mysqli_fetch_row($review)){
			echo "<div class = comment>";
					echo "<td><h5>".$row[1]."</h5></td>";
					echo "commented on ";
					echo "<td>".$row[2]."</td><br><br>";
					echo "<td>".$row[3]."</td><br><br>";
			echo "</div>";
		}
	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if($_POST['command'] === 'insert') {
			setReview();
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <title>BookaBookoo</title>
      <link rel="stylesheet" type="text/css" href="materialize.min.css">
      <link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">      
   </head>
   
   <body>
		<nav class="cyan darken-2">
			<div class="nav-wrapper">
				<img src="src/Bookabookoo.png" class="brand-logo" alt="buku" width="300">
				<ul class="right">
					<li><a href="bukuUser.php">Home</a></li>
					<li><a href="logout.php">Sign Out</a></li>
				</ul>
			</div>
		</nav>
		<div class="container">
			<?php	
     	
		 $books = selectAllFromTable('book');
		 while ($row = mysqli_fetch_row($books)) {
							
		 echo "<tr class='description'>";
			echo "<td><h3>".$row[2]."</h3></td><br>";
			echo "<td><img src=".$row[1]." width='20%' /></td><br>";
			echo "<h6> author</h6>";
			echo "<td> ".$row[3]."</td><br>";
			echo "<h6> publisher</h6>";
			echo "<td> ".$row[4]."</td><br>";
			echo "<h6> description</h6>";			
			echo "<td> ".$row[5]."</td><br><br>";
			echo "<td> quantity: ".$row[6]."</td><br><br>";
		 echo "</tr>";
		}

		 $books = selectAllFromTable($_GET['id']);
		 while ($row = mysqli_fetch_row($books)) {
		if(isset($_SESSION['id_user'])){
		 	$userid = $_SESSION['id_user'];
					echo "<td><form method='POST' action='deskripsi.php?id=".$row[0]."'>
      		<input type='hidden' name='user_id' value='".$userid."'>
      		<input type='hidden' name='book_id' value='".$row[0]."'>
   	  		<textarea name='content'></textarea>
   	  		<input type='hidden' id='insert-command' name='command' value='insert'><br>
			<button type='submit' name = 'reviewSubmit' class='btn btn-primary'>Submit</button>
      	</form>
      	</td><br>";
      	
      	getReview();

			}else
        	echo "<script> alert ('You have to be logged in!');</script>";
		}

	?>
	<style type="text/css">
		body{
			font-family: arial;
		}
		.comment{
			background-color: orange;
			width: 845px;
			padding: 20px;
			margin: 4px;
			border-radius: 4px;
		}
		.btn{
			background-color: black;
			color: white;
		}
		h6{
			color: orange;
			font-weight: bold;
		}	
	</style>
		</div>

		<div class="footer-bottom">
		<style type="text/css">
			.btn {
				background-color: orange;
				border: none;
			}
			.btn a:hover {
				background-color: black

			}
		</style>
		<br>
		<br>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="copyright">
						© 2016, Bookabookoo, All rights reserved
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="design">
						 <a href="#">Bookabookoo </a> |  <a target="_blank" href="http://www.scele.cs.ui.ac.id">Web Design & Development by Bookabookoo</a>
					</div>
				</div>
			</div>
		</div>

     
   </body>
</html>