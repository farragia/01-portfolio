<?php
	session_start();
	if(!$_SESSION['flag']){ //if login in session is not set
    		header("location:login.php");
    		die;
		}
	function connectDB() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "tugasakhir";
		
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " + mysqli_connect_error());
		}
		return $conn;
	}
	
	function selectAllFromTable($table) {
		$conn = connectDB();
		
		$sql = "SELECT * FROM $table";
		
		if(!$result = mysqli_query($conn, $sql)) {
			die("Error: $sql");
		}
		mysqli_close($conn);
		return $result;
	}

	function kembaliBuku($id){
		echo "string";
		$conn = connectDB();
		$sql = "SELECT * FROM loan WHERE book_id = $id";

		$result = mysqli_query($conn, $sql);
		//session
		$row = mysqli_fetch_row($result);


		while ($row = mysqli_fetch_row($result)) {
			if($row[1] == $id && $row[2] == $user_id) {
				echo "yeay masuk akhirnya";
				$sql2 = "SELECT book_id, quantity FROM book WHERE book_id = $id";
				$result2 = mysqli_query($conn, $sql2);
				$row2 = mysqli_fetch_array($result2);
				$book_id = $row2[0];
				$quantity = $row2[1] + 1;	
				$sql3 = "UPDATE book SET quantity = '$quantity' WHERE book_id = $book_id";
				$result3 = mysqli_query($conn, $sql3);
				

				$sql4 = "DELETE FROM loan WHERE book_id = $book_id";
				$result4 = mysqli_query($conn, $sql4);
			}
		}
		mysqli_close($conn);
	}

	function pinjamBuku($id){

		$conn = connectDB();

		$sql = "SELECT * FROM book WHERE book_id = $id";

		$result = mysqli_query($conn, $sql);

		$row = mysqli_fetch_array($result);

		$book_id = $row[0];
		$img_path = $row[1];
		$title = $row[2];
		$author = $row[3];
		$publisher = $row[4];
		$description = $row[5];
		$quantity = $row[6] - 1;
		$user_id = $_SESSION['id_user'];

		$sql2 = "INSERT into loan(book_id, user_id) values($book_id, $user_id)";
		$result2 = mysqli_query($conn, $sql2);

		$sql3 = "UPDATE book SET quantity = '$quantity' WHERE book_id = $id";
		$result3 = mysqli_query($conn, $sql3);
		mysqli_close($conn);
	}


	function insertBook() {
		$conn = connectDB();
		
		$title = $_POST['title'];
		$publisher = $_POST['publisher'];
		$description = $_POST['description'];
		$quantity = $_POST['quantity'];
		$img_path = $_POST['img_path'];
		
		$sql = "INSERT into book (title, publisher, description, quantity, img_path) values('$title','$publisher','$description','$quantity','$img_path')";
		
		if($result = mysqli_query($conn, $sql)) {
			echo "New record created successfully <br/>";
			header("Location: buku.php");
			} else {
			die("Error: $sql");
		}
		mysqli_close($conn);
	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if($_POST['command'] === 'insert') {
			insertBook();
		} else if($_POST['command'] === 'kembalikan') {
			kembaliBuku($_POST['book_id']);
		} else if($_POST['command'] === 'pinjam') {
			pinjamBuku($_POST['book_id']);
		}
	}
	
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>BookaBookoo</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
		<link rel=icon href="src/bookabookoo.png" sizes="16x16" type="image/png">
		<style type="text/css">
			#table2{
				background-color: white;
			}
		</style>
	</head>
	<body>
		<nav class="cyan darken-2">
			<div class="nav-wrapper">
				<img src="src/Bookabookoo.png" class="brand-logo" alt="buku" width="300">
				<ul class="right">
					<li><a href="peminjaman.php">List Peminjaman</a></li>
					<li><a href="logout.php">Sign Out</a></li>
				</ul>
			</div>
		</nav>
		<br>
		<style type="text/css">
			.daftar{
				background-color: black;
				color: white;
				padding : 5px;
			}
		</style>
		<div class="container">
			<div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="insertModalLabel">Add Book</h4>
						</div>
						<div class="modal-body">
							<form action="buku.php" method="post">
								<div class="form-group">
									<label for="title">Judul</label>
									<input type="text" class="form-control" id="insert-title" name="title" placeholder="judul buku">
								</div>
								<div class="form-group">
									<label for="publisher">Penerbit</label>
									<input type="text" class="form-control" id="insert-publish" name="publisher" placeholder="penerbit">
								</div>
								<div class="form-group">
									<label for="description">Deskripsi</label>
									<input type="text" class="form-control" id="insert-desc" name="description" placeholder="deskripsi">
								</div>
								<div class="form-group">
									<label for="quantity">Jumlah</label>
									<input type="text" class="form-control" id="insert-jum" name="quantity" placeholder="quantity">
								</div>
								<div class="form-group">
									<label for="image">Image URL</label>
									<input type="text" class="form-control" id="insert-img" name="img_path" placeholder="url">
								</div>
								<input type="hidden" id="insert-command" name="command" value="insert">
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="table-responsive">
				<table class='table' id="table2">
					<thead> <tr><th>Image</th> <th>Tittle</th> <th>Author</th> <th>Publisher</th><th>Quantity</th></tr> </thead>
					<tbody>
						<?php
							$books = selectAllFromTable("book");
							while ($row = mysqli_fetch_row($books)) {
								echo "<tr>";
									echo "<td><img src=\"$row[1]\" width=\"100%\" /></td>";
									echo "<td><a href=deskripsi.php?id=".$row[0].">".$row[2]."</a></td>";
									echo "<td>".$row[3]."</td>";
									echo "<td>".$row[4]."</td>";
									echo "<td>".$row[6]."</td>";
								if($row[6] > 0){
								echo '<td>
								<form action="buku.php" method="post">
									<input type="hidden" id="delete-roomid" name="book_id" value="'.$row[0].'">
									<input type="hidden" id="delete-command" name="command" value="pinjam">
									<button type="submit" class="btn btn-danger">Pinjam</button>
								</form>
								</td>';
								}
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
			<br />
			<br />
			
			</div>
		</div>
		<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body">
							<form action="buku.php" method="post">
								<div class="form-group">
									<label for="title">Tittle</label>
									<input type="text" class="form-control" id="update-judul" name="judul">
								</div>
								<div class="form-group">
									<label for="autho">Authors</label>
									<input type="text" class="form-control" id="update-pengarang" name="pengarang">
								</div>
								<div class="form-group">
									<label for="publish">Publisher</label>
									<input type="text" class="form-control" id="update-publish" name="publish">
								</div>
								<div class="form-group">
									<label for="deskripsi">Description</label>
									<input type="text" class="form-control" id="update-deskripsi" name="deskripsi">
								</div>
								<div class="form-group">
									<label for="kuantity">Quantity</label>
									<input type="text" class="form-control" id="update-kuantitas" name="kuantitas">
								</div>
								<div class="form-group">
									<label for="image">Image URL</label>
									<input type="text" class="form-control" id="update-image" name="img_path">
								</div>
								<input type="hidden" id="update-bookid" name="book-id">
								<input type="hidden" id="update-command" name="command" value="insert">
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#insertModal" width ="1000">
				Add Book
		</button>
		<style type="text/css">
			 .btn btn-primary {
			 	text-align: center;
			 }

			 body{
			 	text-align: center;
			 }
		</style>
		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<div class="footer-bottom">
		<style type="text/css">
			.btn {
				background-color: orange;
				border: none;
			}
			.btn a:hover {
				background-color: black

			}
		</style>
		<br>
		<br>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="copyright">
						© 2016, Bookabookoo, All rights reserved
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="design">
						 <a href="#">Bookabookoo </a> |  <a target="_blank" href="http://www.scele.cs.ui.ac.id">Web Design & Development by Bookabookoo</a>
					</div>
				</div>
			</div>
		</div>

	</div>
	</body>
</html>